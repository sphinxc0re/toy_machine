use std::sync::mpsc::{self, Receiver, Sender};

use super_emu::{Interface, VM};

struct Console {
    buffer: Vec<u8>,
    sender: Sender<String>,
}

impl Console {
    fn new() -> (Self, Receiver<String>) {
        let (sender, receiver) = mpsc::channel();

        (
            Self {
                buffer: Vec::default(),
                sender,
            },
            receiver,
        )
    }
}

impl Interface for Console {
    fn read(&mut self) -> [u8; super_emu::REGISTER_COUNT] {
        Default::default()
    }

    fn write(&mut self, data: [u8; super_emu::REGISTER_COUNT]) {
        if data[0] > 0 && char::from(data[0]).is_ascii() {
            self.buffer.push(data[0]);
        }

        if data[1] == 1 {
            let line = String::from_utf8(self.buffer.clone())
                .expect("buffer contained invalid characters");

            self.sender.send(line).expect("message to be sent");

            self.buffer.clear();
        }
    }
}

#[test]
fn write_line() {
    let string = b"hello world";
    let mut program: Vec<_> = string
        .into_iter()
        .flat_map(|byte| [0x13, 0, *byte])
        .collect();

    // flush
    program.push(0x13);
    program.push(0x1);
    program.push(0x1);

    // halt
    program.push(0xF0);

    let (inter, recv) = Console::new();
    let mut vm = VM::new(inter, &program);
    vm.run();

    let line = recv.recv().expect("message to arrive");

    assert_eq!(line, "hello world");
}
