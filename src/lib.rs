mod vm;

pub use vm::{
    interface::{Interface, REGISTER_COUNT},
    VM,
};
