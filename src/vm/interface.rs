pub const REGISTER_COUNT: usize = 16;

pub trait Interface {
    fn read(&mut self) -> [u8; REGISTER_COUNT];

    fn write(&mut self, data: [u8; REGISTER_COUNT]);
}
