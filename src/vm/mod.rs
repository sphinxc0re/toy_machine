pub mod interface;

use std::mem;

use self::interface::{Interface, REGISTER_COUNT};

/// number of bytes fetched with each cycle
const FETCH_WINDOW: usize = 8;

// Memory limitations
const MEM_PAGE_SIZE: usize = 1024;
const MAX_PROGRAM_MEMORY: usize = MEM_PAGE_SIZE * 4;
const MAX_WORK_MEMORY: usize = MEM_PAGE_SIZE * 12;
const MEM_SIZE: usize = REGISTER_COUNT + MAX_PROGRAM_MEMORY + MAX_WORK_MEMORY;

pub struct VM<I: Interface> {
    mem: [u8; MEM_SIZE],
    pc: usize,
    sp: usize,
    interface: I,
    halted: bool,
}

impl<I: Interface> VM<I> {
    pub fn new(interface: I, program: &[u8]) -> Self {
        debug_assert!(program.len() < MEM_SIZE);

        let mut mem = [0; MEM_SIZE];
        mem[REGISTER_COUNT..program.len() + REGISTER_COUNT].copy_from_slice(program);

        Self {
            mem,
            pc: REGISTER_COUNT,
            sp: MEM_SIZE - 1,
            interface,
            halted: false,
        }
    }

    pub fn run(&mut self) {
        while !self.halted {
            self.cycle();
        }

        self.halted = false;
    }

    fn halt(&mut self) {
        self.halted = true;
    }

    fn reset(&mut self) {
        // Reset the stack
        self.mem[self.sp..].fill(0);
        self.sp = MEM_SIZE - 1;

        // Reset the program counter
        self.pc = REGISTER_COUNT;
    }

    fn cycle(&mut self) {
        // read data from interface and write into memory
        let interface_data = self.interface.read();
        self.mem[..REGISTER_COUNT].copy_from_slice(interface_data.as_slice());

        let end = (self.pc + FETCH_WINDOW).min(self.sp - FETCH_WINDOW);
        match &self.mem[self.pc..end] {
            // push 8 bit integer onto stack
            &[0x10, byte, ..] => {
                self.push_u8(byte);
                self.pc += 2;
            }
            // push 16 bit integer onto stack
            &[0x11, high_byte, low_byte, ..] => {
                self.push_u8(low_byte);
                self.push_u8(high_byte);
                self.pc += 3;
            }
            // read a byte from a register
            &[0x12, reg_idx, ..] => {
                let reg_idx = reg_idx as usize;
                debug_assert!(reg_idx < REGISTER_COUNT);

                let data = self.mem[reg_idx];
                self.push_u8(data);

                self.pc += 2;
            }
            // write an intermediate byte to a register
            &[0x13, reg_idx, data, ..] => {
                let reg_idx = reg_idx as usize;
                debug_assert!(reg_idx < REGISTER_COUNT);

                self.mem[reg_idx] = data;

                self.pc += 3;
            }
            // write a byte from stack to a register
            &[0x14, reg_idx, ..] => {
                let reg_idx = reg_idx as usize;
                debug_assert!(reg_idx < REGISTER_COUNT);

                let data = self.pop_u8();
                self.mem[reg_idx] = data;
                self.push_u8(data);

                self.pc += 2;
            }
            // 8 bit addition
            &[0x20, ..] => {
                self.add_u8();
                self.pc += 1;
            }
            // 16 bit addition
            &[0x21, ..] => {
                self.add_u16();
                self.pc += 1;
            }
            // halt
            &[0xF0, ..] => {
                self.halt();
                self.pc += 1;
            }
            // stop
            &[0xF1, ..] => {
                self.halt();
                self.reset();
            }
            // reset
            &[0xFF, ..] => self.reset(),
            unknown => panic!("unknown instruction: {unknown:X?}"),
        }

        let mut interface_data = [0; REGISTER_COUNT];
        interface_data
            .as_mut_slice()
            .copy_from_slice(&self.mem[..REGISTER_COUNT]);

        self.interface.write(interface_data);
    }

    fn push_u8(&mut self, data: u8) {
        self.mem[self.sp] = data;
        self.sp -= 1;
    }

    fn pop_u8(&mut self) -> u8 {
        self.sp += 1;

        mem::take(&mut self.mem[self.sp])
    }

    // 8 bit addition
    fn add_u8(&mut self) {
        let a = self.pop_u8();
        let b = self.pop_u8();

        self.push_u8(a.wrapping_add(b))
    }

    // 8 bit addition
    fn add_u16(&mut self) {
        let a_high = self.pop_u8();
        let a_low = self.pop_u8();
        let a = u16::from_be_bytes([a_high, a_low]);

        let b_high = self.pop_u8();
        let b_low = self.pop_u8();
        let b = u16::from_be_bytes([b_high, b_low]);

        let [res_high, res_low] = a.wrapping_add(b).to_be_bytes();

        self.push_u8(res_low);
        self.push_u8(res_high);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    struct DummyInterface;

    impl Interface for DummyInterface {
        fn read(&mut self) -> [u8; REGISTER_COUNT] {
            [0; REGISTER_COUNT]
        }

        fn write(&mut self, _: [u8; REGISTER_COUNT]) {}
    }

    #[test]
    fn test_load_program() {
        const PROGRAM: &[u8] = b"ABCDEFG";
        let vm = VM::new(DummyInterface, PROGRAM);

        assert_eq!(
            &vm.mem[REGISTER_COUNT..PROGRAM.len() + REGISTER_COUNT],
            PROGRAM
        );
    }

    #[test]
    fn test_push_u8() {
        const PROGRAM: &[u8] = &[0x10, 0xff, 0x11, 0xaa, 0xbb];
        let mut vm = VM::new(DummyInterface, PROGRAM);

        // execute both instructions
        vm.cycle();
        vm.cycle();

        // check the stack contents
        assert_eq!(&vm.mem[(vm.sp + 1)..], &[0xaa, 0xbb, 0xff]);
    }

    #[test]
    fn test_add_u8() {
        const PROGRAM: &[u8] = &[0x10, 2, 0x10, 3, 0x20];
        let mut vm = VM::new(DummyInterface, PROGRAM);

        // execute instructions
        vm.cycle();
        vm.cycle();
        vm.cycle();

        // check the stack contents
        assert_eq!(&vm.mem[(vm.sp + 1)..], &[5]);
    }

    #[test]
    fn test_add_u16() {
        let [a_high, a_low] = 3105_u16.to_be_bytes();
        let [b_high, b_low] = 5012_u16.to_be_bytes();

        let program: &[u8] = &[0x11, a_high, a_low, 0x11, b_high, b_low, 0x21];

        let mut vm = VM::new(DummyInterface, program);

        // execute instructions
        vm.cycle();
        vm.cycle();
        vm.cycle();

        // check the stack contents

        let [res_high, res_low] = 8117_u16.to_be_bytes();
        assert_eq!(&vm.mem[(vm.sp + 1)..], &[res_high, res_low]);
    }

    #[test]
    fn test_reset() {
        const PROGRAM: &[u8] = &[0x10, 2, 0x10, 3, 0xFF];
        let mut vm = VM::new(DummyInterface, PROGRAM);

        vm.cycle();
        vm.cycle();

        assert_eq!(&vm.mem[(vm.sp + 1)..], &[3, 2]);

        // reset
        vm.cycle();

        assert_eq!(&vm.mem[(vm.sp + 1)..], &[]);
    }

    #[test]
    #[should_panic]
    fn test_unknown_instruction() {
        const PROGRAM: &[u8] = &[0x10, 0xFE, 0xFE];
        let mut vm = VM::new(DummyInterface, PROGRAM);
        vm.run();
    }

    #[test]
    fn test_run_and_halt() {
        const PROGRAM: &[u8] = &[0x10, 2, 0x10, 3, 0x20, 0xF0];
        let mut vm = VM::new(DummyInterface, PROGRAM);
        vm.run();
    }
}
