mod vm;

use std::{fs, path::PathBuf};

use anyhow::Result;
use clap::Parser;
use vm::{
    interface::{Interface, REGISTER_COUNT},
    VM,
};

#[derive(Parser)]
struct Cmd {
    /// The program to be run
    program: PathBuf,
}

fn main() -> Result<()> {
    let cmd = Cmd::parse();

    let program = fs::read(&cmd.program)?;

    let mut vm = VM::new(DummyInterface, program.as_slice());
    vm.run();

    Ok(())
}

struct DummyInterface;

impl Interface for DummyInterface {
    fn read(&mut self) -> [u8; REGISTER_COUNT] {
        [0; REGISTER_COUNT]
    }

    fn write(&mut self, _: [u8; REGISTER_COUNT]) {}
}
